﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson20_HW.PersonClasses
{
    class PersonComarerByID : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.ID - y.ID;
        }
    }
}
