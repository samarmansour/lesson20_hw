﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lesson20_HW.PersonClasses
{
    class Person : IComparable<Person>
    {
        public int ID { get; private set; }
        public int Age { get; private set; }
        public float Height { get; private set; }
        public string Name { get; private set; }

        private static readonly IComparer<Person> idComparer;
        private static readonly IComparer<Person> ageComparer;
        private static readonly IComparer<Person> heightComparer;
        private static readonly IComparer<Person> nameComparer;

        public static IComparer<Person> IDComparer
        {
            get
            {
                return idComparer;
            }
        }

        public static IComparer<Person> AgeComparer
        {
            get
            {
                return ageComparer;
            }
        }
        public static IComparer<Person> HeightComparer
        {
            get
            {
                return heightComparer;
            }
        }
        public static IComparer<Person> NameComparer
        {
            get
            {
                return nameComparer;
            }
        }

        // ***Challenge
        private static IComparer<Person> DefualtCompare;
        public static void ModifyDefaultComparer(IComparer<Person> otherComparer)
        {
            DefualtCompare = otherComparer;
        }
        public static IComparer<Person> GetDefaultComparer()
        {
            return DefualtCompare;
        }

        public Person(int id, int age, float height, string name)
        {
            ID = id;
            Age = age;
            Height = height;
            Name = name;
        }

        static Person()
        {
            idComparer = new PersonComarerByID();
            ageComparer = new PersonCompareByAge();
            heightComparer = new PersonCompareByHeight();
            nameComparer = new PersonCompareByName();

            DefualtCompare = idComparer;
        }


        public override string ToString()
        {
            return $"{base.ToString()} ID: {this.ID}   Name: {this.Name}   Age: {this.Age}   Height: {this.Height}";
        }

        public int CompareTo(Person person)
        {
            //Before Challenge - 2 DefaultComparer
            //return ID - person.ID;

            //After Challenge - 2 
            return DefualtCompare.Compare(this, person);
        }
    }
}
