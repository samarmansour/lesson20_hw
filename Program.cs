﻿using Lesson20_HW.PersonClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson20_HW
{
    class Program
    {
        static void PrintPersonArray(Person[] person)
        {
            foreach (Person p in person)
            {
                Console.WriteLine(p);
            }
        }
        static void Main(string[] args)
        {
            //Part A
            Person[] people = new Person[5]
            {
                new Person(3, 55, 1.7f, "Juan"),
                new Person(2, 37, 1.8f, "Salam"),
                new Person(5, 45, 1.73f, "Sabien"),
                new Person(4, 60, 1.69f, "Tammy"),
                new Person(1, 18, 1.55f, "Kamel"),
            };
            
            Console.WriteLine("Before Sort:");
            PrintPersonArray(people);


            Console.WriteLine("======================================");
            Console.WriteLine("Default Sort by ID:");
            Array.Sort(people);
            PrintPersonArray(people);

            //*Challenge - 1
            Console.WriteLine("======================================");
            Console.WriteLine("Sort By Name:");
            Array.Sort(people, new PersonCompareByName());
            PrintPersonArray(people);

            //Part B
            Console.WriteLine("======================================");
            Console.WriteLine("Part Two: Sorting with static members:");
            Console.WriteLine("Sort By NameComparer");
            Array.Sort(people, Person.NameComparer);
            PrintPersonArray(people);

            Console.WriteLine("======================================");
            Console.WriteLine("Sort By IDComparer");
            Array.Sort(people, Person.IDComparer);
            PrintPersonArray(people);

            Console.WriteLine("======================================");
            Console.WriteLine("Sort By AgeComparer");
            Array.Sort(people, Person.AgeComparer);
            PrintPersonArray(people);

            Console.WriteLine("======================================");
            Console.WriteLine("Sort By HeightComparer");
            Array.Sort(people, Person.HeightComparer);
            PrintPersonArray(people);

            //***Challenge - 2
            Console.WriteLine("======================================");
            Console.WriteLine("Sort By DefaultComparer By ID:");
            Array.Sort(people);
            PrintPersonArray(people);
            Console.WriteLine();
            Console.WriteLine("Sort By DefaultComparer By Name:");
            Person.ModifyDefaultComparer(Person.NameComparer);
            Array.Sort(people);
            PrintPersonArray(people);
        }
    }
}
